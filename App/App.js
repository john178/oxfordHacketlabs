/* import Home from '../App/src/component/Home';
import Chat from '../App/src/component/Chat';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation-tabs';

const TabNavigator = createBottomTabNavigator({
  Home: { screen: Home },
  Chat: { screen: Chat },
});

export default createAppContainer(TabNavigator); */
import Home from '../App/src/component/Home';
import Booking from '../App/src/component/Booking';
import Chat from '../App/src/component/Chat';
import Search from '../App/src/component/Search';
import Contacts from '../App/src/component/Contacts'

import {
  geocodeByAddress,
  geocodeByPlaceId,
  getLatLng,
} from 'react-places-autocomplete';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

const MainNavigator = createStackNavigator({
  Home: { screen: Home },
  Chat: { screen: Chat },
  Booking: { screen: Booking },
  Search: { screen: Search},
  Contacts: { screen: Contacts},
});

const App = createAppContainer(MainNavigator);

export default App; 