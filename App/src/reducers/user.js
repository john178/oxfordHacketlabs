import {
    LOGIN_SUCESS
} from '../actions/index';
import { statement } from '@babel/template';

let cloneObject = function(obj){
    return JSON.parse(JSON.stringify(obj))
}

let newState = { user: { loggedIn: false } };

export default function (state, action){
    switch(action.type){
        case LOGIN_SUCESS:
            newState = cloneObject(state);
            newState.user.loggedIn = true;
            return newState;
            default:
                return state || newState;
    }
};