import React from 'react';
import { Button } from 'react-native';
import { StyleSheet, Text, View } from 'react-native';
import Timeline from 'react-native-timeline-flatlist';

 class Booking extends React.Component {
    static navigationOptions = {
        title: "Appointment History"
      }
      constructor() {
        super();
        this.data = [
          {
            time: '09:00',
            title: 'Dr Ben Tunner ',
            description:
              'Results checkup',  
          },
          {
            time: '10:45',
            title: 'Dr Stevens',
            description:
              'Nutrition plan',
          },
        ];
      }
      render() {
        const { navigate } = this.props.navigation;
      return (
        <Button
        title="Chat"
        onPress={() => navigate("Search", {screen: 'Search'})}
      />
       // <Timeline style={{ flex: 1 }} data={this.data} />
      );
    }
 }

  export default Booking;