import React from 'react';
import { Button, View, TextInput } from 'react-native';
import {
    geocodeByAddress,
    getLatLng,
  } from 'react-places-autocomplete';
import LocationSearchInput from '../component/LocationSearch';  

class Search extends React.Component {
    static navigationOptions = {
        title: "Search Appointment"
      }
      constructor() {
        super();
        this.state = { address: '' };
      }

      handleChange = address => {
        this.setState({ address });
      };
     
      handleSelect = address => {
        geocodeByAddress(address)
          .then(results => getLatLng(results[0]))
          .then(latLng => console.log('Success', latLng))
          .catch(error => console.error('Error', error));
      };

      render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{padding: 10}}>

            <TextInput
            style={{height: 40, borderColor: 'black', borderRadius: 4, width: 200}}
            placeholder="Try search dentist,flu,diet or speciality"
            />
            <TextInput
            style={{height: 40, borderColor: 'black', borderRadius: 4, width: 200}}
            placeholder="Search Address,zip code or city"
            />
            <TextInput
            style={{height: 40, borderColor: 'black', borderRadius: 4, width: 200}}
            placeholder="Search Available Date"
            />
            <Button
            style={{height: 40, borderColor: 'black', borderRadius: 4, width: 200}}
                title="Find Appointment"
                onPress={() => navigate("Contacts", {screen: 'Contacts'})}
      />
      </View>
        );
    }
 }

  export default Search;