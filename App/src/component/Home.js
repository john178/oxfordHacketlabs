import React from 'react';
import Chat from './Chat';
import { Button } from 'react-native';

 class Home extends React.Component {
    static navigationOptions = {
        title: "Welcome"
      }
      render() {
        const { navigate } = this.props.navigation;
      return (

        <Button
        title="Booking"
        onPress={() => navigate("Booking", {screen: 'Booking'})}
      />
      );
    }
  } 
/*   class HomeScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Home!</Text>
        </View>
      );
    }
  } */
  export default Home;