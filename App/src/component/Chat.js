import React from 'react'
import { GiftedChat } from 'react-native-gifted-chat'

class Chat extends React.Component {
  state = {
    messages: [],
  }
    static navigationOptions = ({ navigation }) => ({
      title: (navigation.state.params || {}).name || 'Chat!',
    });
  
    state = {
      messages: [],
    };
  
  /*   get user() {
      return {
        name: this.props.navigation.state.params.name,
        _id: Fire.shared.uid,
      };
    } */
    static navigationOptions = ({ navigation }) => {
        return {
          title: `Dr Stevens`,
        }
      };
  
      componentWillMount() {
        this.setState({
          messages: [
            {
              _id: 1,
              text: 'Hello Jane',
              createdAt: new Date(),
              user: {
                _id: 2,
                name: 'React Native',
                avatar: 'https://placeimg.com/140/140/any',
              },
            },
            {
              _id: 1,
              text: 'How can I help you?',
              createdAt: new Date(),
              quickReplies: {
                type: 'radio', // or 'checkbox',
                keepIt: true,
                values: [
                ],
              },
              user: {
                _id: 2,
                name: 'React Native',
              },
            },
          ],
        })
      }
    
      onSend(messages = []) {
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, messages),
        }))
      }
    
      render() {
        return (
          <GiftedChat
            messages={this.state.messages}
            onSend={messages => this.onSend(messages)}
            user={{
              _id: 1,
            }}
          />
        )
      }
    }
  
 /*    componentDidMount() {
      Fire.shared.on(message =>
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, message),
        }))
      );
    } */

  
  
  export default Chat;